<?php

namespace Drupal\competition_video\Form;

use Drupal;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the competition_video entity edit forms.
 */
class CompetitionVideoForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()
        ->addStatus($this->t('New competition video %label has been created.', $message_arguments));
      $this->logger('competition_video')
        ->notice('Created new competition video %label', $logger_arguments);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('The competition video %label has been updated.', $message_arguments));
      $this->logger('competition_video')
        ->notice('Updated new competition video %label.', $logger_arguments);
    }

    $user = Drupal::currentUser();
    if ($user->id() > 0) {
      $form_state->setRedirect('entity.competition_video.canonical', ['competition_video' => $entity->id()]);
    }
    else {
      $form_state->setRedirect('competition_video.submitted_form', ['competition_video' => $entity->id()]);
    }
  }

}
