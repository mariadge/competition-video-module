<?php

namespace Drupal\competition_video;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\File\FileUrlGenerator;

/**
 * Provides a list controller for the competition_video entity type.
 */
class CompetitionVideoListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * FileUrlGenerator service.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a new CompetitionVideoListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   The url generator service.
   */
  public function __construct(EntityTypeInterface          $entity_type,
                              EntityStorageInterface       $storage,
                              DateFormatterInterface       $date_formatter,
                              RedirectDestinationInterface $redirect_destination,
                              FileUrlGenerator             $fileUrlGenerator) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total competition_videos: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    $header['lastname'] = $this->t('Lastname');
    $header['gender'] = $this->t('Gender');
    $header['email'] = $this->t('E-mail');
    $header['zipcode'] = $this->t('Zip code');
    $header['video'] = $this->t('Video');
    $header['video_url'] = $this->t('Watch video');
    $header['created'] = $this->t('Created');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\competition_video\CompetitionVideoInterface */
    $file = $entity->video->entity;
    $fileLocation = $file->getFileUri();
    $fileUrl = $this->fileUrlGenerator
      ->generateAbsoluteString($fileLocation);
    $url = Url::fromUri($fileUrl, ['attributes' => ['target' => '_blank']]);
    $link = Link::fromTextAndUrl(t('@title', ['@title' => 'Watch']), $url)
      ->toString();
    $row['id'] = $entity->toLink();
    $row['name'] = $entity->name->value;
    $row['lastname'] = $entity->lastname->value;
    $row['gender'] = $entity->gender->value;
    $row['email'] = $entity->email->value;
    $row['zipcode'] = $entity->zipcode->value;
    $row['video'] = $file->getFileName();
    $row['video_url'] = $link;
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach ($operations as $key => $operation) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

}
