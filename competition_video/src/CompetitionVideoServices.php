<?php

namespace Drupal\competition_video;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGenerator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Symfony\Component\HttpFoundation\Response;

/**
 * @file providing the services needed by the competition_video module
 *
 */
class CompetitionVideoServices {

  /**
   * EntityTypeManagerInterface service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * FileUrlGenerator service.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a new CompetitionService.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   The url generator service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
                              FileUrlGenerator           $fileUrlGenerator) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * Function that returns a response with the headers for the needed file
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response with headers to be used for the export of competition videos
   */
  public function makeResponse() {
    $response = new Response();
    $response->headers->set('Pragma', 'no-cache');
    $response->headers->set('Expires', '0');
    $response->headers->set('Content-Type', 'application/vnd.ms-excel');
    $response->headers->set('Content-Disposition', 'attachment; filename=video_competition_data.xlsx');

    return $response;
  }

  /**
   * Function that sets the spreadsheet properties (metadata)
   *
   * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
   *   Spreadsheet to be used for the export of competition videos
   */
  public function makeSpreadsheet() {
    $spreadsheet = new Spreadsheet();
    $spreadsheet->getProperties()
      ->setCreator('Submission user')
      ->setLastModifiedBy('Submission user')
      ->setTitle("Video submissions data")
      ->setDescription('File with the video competition data')
      ->setSubject('Video competition data')
      ->setKeywords('excel php office phpexcel video competition data')
      ->setCategory('data');

    return $spreadsheet;
  }

  /**
   * Function that creates the header row of the spreadsheet (with styling)
   *
   * @param $spreadsheet
   *   Variable containing the spreadsheet
   *
   * @return worksheet element of \PhpOffice\PhpSpreadsheet\Spreadsheet
   *   Worksheet to be used for the export of competition videos
   */
  public function makeWorksheetHeader($spreadsheet) {
    $spreadsheet->setActiveSheetIndex(0);
    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getStyle('A1:G1')
      ->getFill()
      ->setFillType(Fill::FILL_SOLID)
      ->getStartColor()
      ->setARGB('085efd');
    $styleArrayHead = [
      'font' => [
        'bold' => TRUE,
        'color' => ['rgb' => 'ffffff'],
      ],
    ];
    $worksheet->getCell('A1')->setValue('E-mail');
    $worksheet->getCell('B1')->setValue('Geschlecht');
    $worksheet->getCell('C1')->setValue('Vorname');
    $worksheet->getCell('D1')->setValue('Nachname');
    $worksheet->getCell('E1')->setValue('PLZ');
    $worksheet->getCell('F1')->setValue('Video name');
    $worksheet->getCell('G1')->setValue('Video URL');
    $worksheet->getStyle('A1:G1')->applyFromArray($styleArrayHead);

    return $worksheet;
  }

  /**
   * Function that gets all the submissions from database (competition_video
   * entities)
   *
   * @return array
   *   Array of all competition_video entities for the export of competition
   *   videos
   */
  public function getSubmissions() {
    $submissions = $this->entityTypeManager
      ->getStorage('competition_video')
      ->loadMultiple();

    return $submissions;
  }

  /**
   * Function that fills in the content rows of the worksheet
   *
   * @param $submissions
   *   All the records in database
   * @param $worksheet
   *   Variable containing the active worksheet
   *
   * @return NULL
   */
  public function fillContent($submissions, $worksheet) {
    // Data will be added from row 4 (after header)
    $i = 2;
    foreach ($submissions as $submission) {
      // The file_id for the video and the public URI
      $videoId = $submission->video->getValue()[0]['target_id'];
      $video = $this->entityTypeManager->getStorage('file')->load($videoId);
      $fileName = $video->getFilename();
      $fileLocation = $video->getFileUri();
      $fileUrl = $this->fileUrlGenerator
        ->generateAbsoluteString($fileLocation);
      $worksheet->setCellValue('A' . $i, $submission->email->getValue()[0]['value']);
      $worksheet->setCellValue('B' . $i, $submission->gender->getValue()[0]['value']);
      $worksheet->setCellValue('C' . $i, $submission->name->getValue()[0]['value']);
      $worksheet->setCellValue('D' . $i, $submission->lastname->getValue()[0]['value']);
      $worksheet->setCellValue('E' . $i, $submission->zipcode->getValue()[0]['value']);
      $worksheet->setCellValue('F' . $i, $fileName);
      $worksheet->setCellValue('G' . $i, $fileUrl);
      $worksheet->getCell('G' . $i)
        ->getHyperlink()
        ->setUrl($fileUrl);
      $i++;
    }
    // Set autowidth for all the columns
    foreach (range('A', 'G') as $col) {
      $worksheet->getColumnDimension($col)->setAutoSize(TRUE);
    }

    return NULL;
  }

}
