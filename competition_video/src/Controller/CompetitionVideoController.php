<?php

namespace Drupal\competition_video\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Drupal\competition_video\CompetitionVideoServices;

/**
 * CompetitionVideoController class.
 */
class CompetitionVideoController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * CompetitionVideoServices definition.
   *
   * @var \Drupal\competition_video\CompetitionVideoServices;
   */
  protected $competitionVideoServices;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\EntityFormBuilder
   */
  protected $entityFormBuilder;

  /**
   * Constructs a new CompetitionVideoController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The EntityTypeManagerInterface service.
   * @param \Drupal\competition_video\CompetitionVideoServices $competitionVideoServices
   *   The CompetitionVideo Services
   * @param \Drupal\Core\Form\EntityFormBuilder $entityFormBuilder
   *   The form builder.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
                              CompetitionVideoServices   $competitionVideoServices,
                              EntityFormBuilder          $entityFormBuilder) {
    $this->entityTypeManager = $entityTypeManager;
    $this->competitionVideoServices = $competitionVideoServices;
    $this->entityFormBuilder = $entityFormBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('competition_video.services'),
      $container->get('entity.form_builder')
    );
  }

  /**
   * Function displays the form to make a submission to the video competition
   *
   * @return array
   *   The video submission form.
   */
  public function videoSubmissionPage() {
    $video = $this->entityTypeManager->getStorage('competition_video')
      ->create(['type' => 'competition_video']);

    $form = $this->entityFormBuilder->getForm($video);

    return $form;
  }

  /**
   * Function displays the message for the user after the submission to the
   * video competition
   *
   * @return array
   *   The video submission form thank you page.
   */
  public function videoSubmissionCompleted() {
    $page = [
      '#title' => $this->t('Video competition submission'),
      '#markup' => $this->t('Vielen Dank für Ihre Teilnahme!'),
    ];
    return $page;
  }

  /**
   * Function makes the excel file with the video competition data
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response with the file for the export of competition videos
   */
  public function fileDownloadPage() {
    $response = $this->competitionVideoServices->makeResponse();
    $spreadsheet = $this->competitionVideoServices->makeSpreadsheet();
    $worksheet = $this->competitionVideoServices->makeWorksheetHeader($spreadsheet);
    $submissions = $this->competitionVideoServices->getSubmissions();
    $this->competitionVideoServices->fillContent($submissions, $worksheet);
    // Get the writer and export in memory.
    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    ob_start();
    $writer->save('php://output');
    $content = ob_get_clean();
    // Memory cleanup.
    $spreadsheet->disconnectWorksheets();
    unset($spreadsheet);
    // Retrieve the file data
    $response->setContent($content);

    return $response;
  }

}


