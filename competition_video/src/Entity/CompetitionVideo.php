<?php

namespace Drupal\competition_video\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\competition_video\CompetitionVideoInterface;

/**
 * Defines the competition_video entity class.
 *
 * @ContentEntityType(
 *   id = "competition_video",
 *   label = @Translation("Competition video"),
 *   label_collection = @Translation("Competition videos"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\competition_video\CompetitionVideoListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" =
 *   "Drupal\competition_video\CompetitionVideoAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\competition_video\Form\CompetitionVideoForm",
 *       "default" = "Drupal\competition_video\Form\CompetitionVideoForm",
 *       "edit" = "Drupal\competition_video\Form\CompetitionVideoForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "competition_video",
 *   data_table = "competition_video_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer competition_video",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/competition-video/add",
 *     "canonical" = "/competition_video/{competition_video}",
 *     "edit-form" =
 *   "/admin/content/competition-video/{competition_video}/edit",
 *     "delete-form" =
 *   "/admin/content/competition-video/{competition_video}/delete",
 *     "collection" = "/admin/content/competition-video"
 *   },
 *   field_ui_base_route = "entity.competition_video.settings"
 * )
 */
class CompetitionVideo extends ContentEntityBase implements CompetitionVideoInterface {

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    // Add email field
    $fields['email'] = BaseFieldDefinition::create('email')
      ->setRequired(TRUE)
      ->setLabel(t('E-mail'))
      ->setDescription(t('The email of the user that uploaded the video.'))
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'email_mailto',
        'label' => 'above',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE);
    // Add gender field
    $fields['gender'] = BaseFieldDefinition::create('list_string')
      ->setRequired(TRUE)
      ->setLabel(t('Geschlecht'))
      ->setDescription(t('The name of the user that uploaded the video.'))
      ->setSettings([
        'allowed_values' => [
          'weiblich' => 'weiblich',
          'männlich' => 'männlich',
          'divers' => 'divers',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'list_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);
    // Add name field
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Vorname'))
      ->setDescription(t('The name of the user that uploaded the video.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'above',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);
    // Add lastname field
    $fields['lastname'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Nachname'))
      ->setDescription(t('The lastname of the user that uploaded the video'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'above',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);
    // Add zipcode field
    $fields['zipcode'] = BaseFieldDefinition::create('integer')
      ->setRequired(TRUE)
      ->setLabel(t('PLZ'))
      ->setDescription(t('The zipcode of the user that uploaded the video'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'label' => 'above',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('view', TRUE);
    // Add video field
    $validators = [
      'file_validate_extensions' => ['mp4 mov mkv avi wmv'],
      'file_validate_size' => ['500000000'],
    ];
    $fields['video'] = BaseFieldDefinition::create('file')
      ->setRequired(TRUE)
      ->setLabel(t('Video'))
      ->setDescription(t('The video for the competition.'))
      ->setSetting('upload_validators', $validators)
      ->setSetting('max_filesize', '500 MB')
      ->setSetting('file_directory', 'competition_videos')
      ->setSetting('file_extensions', 'mp4 mov mkv avi wmv')
      ->setSetting('uri_scheme', 'public')
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'file_default',
        'label' => 'above',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('view', TRUE);
    // Add created field - hidden for user
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time of the video submission.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 35,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
