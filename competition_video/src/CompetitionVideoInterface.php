<?php

namespace Drupal\competition_video;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a competition_video entity type.
 */
interface CompetitionVideoInterface extends ContentEntityInterface {

  /**
   * Gets the competition_video creation timestamp.
   *
   * @return int
   *   Creation timestamp of the competition_video.
   */
  public function getCreatedTime();

  /**
   * Sets the competition_video creation timestamp.
   *
   * @param int $timestamp
   *   The competition_video creation timestamp.
   *
   * @return \Drupal\competition_video\CompetitionVideoInterface
   *   The called competition_video entity.
   */
  public function setCreatedTime($timestamp);

}
