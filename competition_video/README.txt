CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Routing


INTRODUCTION
------------

Custom Entity module with a form where a user can participate in a video competition.
Module allows admins to export the input data to an excel file.


REQUIREMENTS
------------

This module requires the following module:

 * phpoffice/phpspreadsheet - https://github.com/PHPOffice/PhpSpreadsheet


RECOMMENDED MODULES
-------------------

  * No extra module is required.

INSTALLATION
------------

 * Install as you would normally install a custom Drupal module.


CONFIGURATION
-------------

 No additional configuration is needed.
 Users that will work with the module need permission to admin the entities ("administer competition_video")


ROUTING
-----------

 * Route to upload a video: /competition-video
 * Admin page route to see the video collection or download the excel file: /admin/content/competition-video
